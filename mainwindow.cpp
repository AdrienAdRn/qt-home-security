#include "mainwindow.h"
#include <QDebug>
#include <QList>
#include <QCamera>
#include <QCameraInfo>
#include <QMessageBox>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/videoio.hpp"
#include <QImage>
#include <QPixmap>
#include "Utilities.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), fileMenu(nullptr), videoThread(nullptr)
{
    initUI();
    data_lock = new QMutex();
}

MainWindow::~MainWindow()
{
//    delete ui;
}

void MainWindow::initUI()
{
    // overall window
    this->resize(750,600);

    //toolbar
    fileMenu = menuBar()->addMenu(tr("&File"));

    // video viewer area
    QGridLayout *mainLayout = new QGridLayout();
    videoScene = new QGraphicsScene(this);
    videoView = new QGraphicsView(videoScene);
    mainLayout->addWidget(videoView,0,0,12,1); // addWidget(*Widget, row, column, rowspan, colspan)

    // user tools area
    QGridLayout *userToolsLayout = new QGridLayout();
    mainLayout->addLayout(userToolsLayout,12,0,1,1);

    monitorBox = new QCheckBox(this);
    monitorBox->setText("Monitor On/Off");
    userToolsLayout->addWidget(monitorBox,0,3,1,1);
//    QObject::connect(monitorBox,SIGNAL(stateChanged(int)),this,SLOT(updateMonitorBoxStatus(int));

    recordButton = new QPushButton(this);
    recordButton->setText("Record");
    userToolsLayout->addWidget(recordButton,0,1,1,2,Qt::AlignHCenter|Qt::AlignVCenter);
    //userToolsLayout->addWidget(new QLabel(this),0,3,1,1); // spacer
    //userToolsLayout->addWidget(new QLabel(this),0,0,1,1);
    QObject::connect(recordButton,&QPushButton::clicked,this,&MainWindow::recordingStartStop);

    // video list
    recordList = new QListView(this);
    userToolsLayout->addWidget(recordList,13,0,4,4);

    // apply Layout to main window
    QWidget *ui_area = new QWidget();
    ui_area->setLayout(mainLayout);
    this->setCentralWidget(ui_area);

    // setup status bar
    mainStatusBar = statusBar();
    mainStatusLabel = new QLabel(mainStatusBar);
    mainStatusBar->addPermanentWidget(mainStatusLabel);
    mainStatusLabel->setText("Home Security is Ready");

    createActions();


}

void MainWindow::createActions()
{
    cameraInfoAction = new QAction("Camera &Information", this);
    fileMenu->addAction(cameraInfoAction);
    openCameraAction = new QAction("&Open Camera", this);
    fileMenu->addAction(openCameraAction);
    exitAction = new QAction("Exit", this);
    fileMenu->addAction(exitAction);
    calcFpsAction = new QAction("&Calculate FPS",this);
    fileMenu->addAction(calcFpsAction);

    // connecting signal slots
    QObject::connect(cameraInfoAction,&QAction::triggered,this,&MainWindow::displayCamerasInfo);
    QObject::connect(openCameraAction,&QAction::triggered,this,&MainWindow::openCamera);
    QObject::connect(calcFpsAction,&QAction::triggered,this,&MainWindow::calculateFPS);
}

void MainWindow::displayCamerasInfo(){
    QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
    QString info = ("Available Camera: \n");

    for(auto cameraInfo: cameras){
        info += " - device name : " + cameraInfo.deviceName() + "\n";
        info += " - device description : " + cameraInfo.description() + ".\n";

        QMessageBox::information(this,"Cameras",info);
    }

}

void MainWindow::openCamera()
{
    qInfo() << "Info : openCamera slot";
    // check that we launch only 1 capture thread at a time
    if(videoThread!=nullptr){

        // if true we stop the current thread before creating a new one
        videoThread->setRunning(false);
        QObject::disconnect(videoThread,&CaptureThread::frameCaptured,this,&MainWindow::updateCameraFrame);
        QObject::disconnect(videoThread,&CaptureThread::fpsChanged,this,&MainWindow::updateFPS);
        QObject::disconnect(videoThread,&CaptureThread::statusLabelChange,this,&MainWindow::updateStatusLabel);
        QObject::disconnect(videoThread, &CaptureThread::videoRecorded, this, &MainWindow::appendRecordedVideo);

        QObject::connect(videoThread,&CaptureThread::finished,videoThread,&CaptureThread::deleteLater);       
    }

    int cameraIndex = 0;
    videoThread = new CaptureThread(cameraIndex, data_lock);
    qInfo() << "Info : Capture Thread created.";

    // connect the GUI frame with the parallel thread frame acquisition
    connect(videoThread,&CaptureThread::frameCaptured,this,&MainWindow::updateCameraFrame);
    connect(videoThread,&CaptureThread::fpsChanged,this,&MainWindow::updateFPS);
    connect(videoThread,&CaptureThread::statusLabelChange,this,&MainWindow::updateStatusLabel);
    connect(videoThread, &CaptureThread::videoRecorded, this, &MainWindow::appendRecordedVideo);
    videoThread->start();

}

void MainWindow::updateCameraFrame(cv::Mat *mat)
{
    // --- update frame data ---
    // prevent data race conditions between GUI thread and  capture thread
    data_lock->lock();
    currentFrame = *mat;
    data_lock->unlock();

    // --- display current frame into the GUI ---
    QImage frameDisplay(
        currentFrame.data,
        currentFrame.cols,
        currentFrame.rows,
        currentFrame.step,
        QImage::Format_RGB888); // 24-bit RGB format (8-8-8)

    QPixmap imageDisplay = QPixmap::fromImage(frameDisplay);
    videoScene->clear();
    videoView->resetTransform();
    videoScene->addPixmap(imageDisplay);
    videoScene->update();
    videoView->setSceneRect(imageDisplay.rect());
}

void MainWindow::calculateFPS()
{
    qInfo() << "INFO : user order to compute the fps.";
    if(videoThread!=nullptr){
        videoThread->startCalcFPS();
    } else {
        const QString info = "FPS calculus requires the video stream to be open.";
        QMessageBox::information(this,"Compute FPS requirement",info);
    }
}

void MainWindow::updateFPS(float fps)
{
    mainStatusLabel->setText(QString("FPS of the current camera is : %1").arg(fps));
}

void MainWindow::updateStatusLabel(const QString message)
{
    mainStatusLabel->setText(message);
    //mainStatusLabel->setText(QString("Capturing camera %1").arg(cameraIndex));
    qInfo("Info : status label changed %s",qPrintable(message));
}

void MainWindow::recordingStartStop()
{
    if(videoThread == nullptr){
        const QString text = "Recording videos is enabled only when video stream is open.";
        QMessageBox::information(this,"Video Recording Requirement",text);
        return;
    }

    // record button as a ON/OFF feature
    QString buttonText = recordButton->text();
    if(buttonText == "Record" && videoThread !=nullptr){
        videoThread->setVideoSavingStatus(CaptureThread::STARTING);
        recordButton->setText("Stop Recording");
    }

    if(buttonText == "Stop Recording" && videoThread !=nullptr){
        videoThread->setVideoSavingStatus(CaptureThread::STOPPING);
        recordButton->setText("Record");
    }

}

void MainWindow::appendRecordedVideo(const QString name)
{
 // TODO
}

//void MainWindow::updateMonitorBoxStatus(int status)
//{

//}
