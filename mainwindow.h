#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QListView>
#include <QCheckBox>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QStatusBar>
#include <QLabel>
#include <QGridLayout>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QMutex>
#include "CaptureThread.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void initUI();
    void createActions();

private slots:
    void displayCamerasInfo();
    void openCamera();
    void updateCameraFrame(cv::Mat *mat);
    void calculateFPS();
    void updateFPS(float fps);
    void updateStatusLabel(const QString message);
    void recordingStartStop();
    void appendRecordedVideo(const QString name);
//    void updateMonitorBoxStatus(int status);


private:
    QMenu *fileMenu;
    QGraphicsScene *videoScene;
    QGraphicsView *videoView;
    QCheckBox *monitorBox;
    QPushButton *recordButton;
    QListView *recordList;
    QLabel *statusLabel;
    QStatusBar *mainStatusBar;
    QLabel *mainStatusLabel;

    //Actions
    QAction *cameraInfoAction;
    QAction *openCameraAction;
    QAction *calcFpsAction;
    QAction *exitAction;

    //streaming
    cv::Mat currentFrame;
    QMutex *data_lock;
    CaptureThread *videoThread;

};

#endif // MAINWINDOW_H
