#ifndef CAPTURETHREAD_H
#define CAPTURETHREAD_H
#include <QThread>
#include <QMutex>
#include <QString>
#include <QTime>
#include "opencv2/opencv.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/core.hpp>


class CaptureThread : public QThread
{
    Q_OBJECT
public:
    CaptureThread(int cameraIndex, QMutex *lock);
    CaptureThread(QString videoPath, QMutex *lock);
    ~CaptureThread();
    void setRunning(bool run) {running = run;}
    void startCalcFPS(){computingFps = true;}

    bool getRunning(){ return running;}

        enum VideoSavingStatus{
            STARTING,
            STARTED,
            STOPPING,
            STOPPED
        };

    void setVideoSavingStatus(VideoSavingStatus status){video_saving_status = status;}

protected:
    void run() override;

signals:
    frameCaptured(cv::Mat *data);
    statusLabelChange(const QString message);
    fpsChanged(float fps);
    videoRecorded(QString name);

private:
    void computeFPS(cv::VideoCapture &cap);
    void startRecordingVideo(cv::Mat &firstFrame);
    void stopRecordingVideo();

    // ---- member variables
    bool running; // boolean controlled by the menu action
    int cameraIndex;
    QString videoPath;
    QMutex *data_lock;
    cv::Mat frame;

    // fps
    bool computingFps;
    float fps;

    // video recording
    int frame_width;
    int frame_height;
    VideoSavingStatus video_saving_status;
    QString name_video_to_save;
    cv::VideoWriter *video_writer;
    int codec;
};

#endif // CAPTURETHREAD_H
