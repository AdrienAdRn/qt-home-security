#include "CaptureThread.h"
#include <qDebug>
#include "Utilities.h"
#include <opencv2/imgcodecs.hpp>

CaptureThread::CaptureThread(int cameraIndex, QMutex *lock) : running(false),
    cameraIndex(cameraIndex), videoPath(""), data_lock(lock)
{
    computingFps = false;
    fps = 0.0;

    //init video recording variables
    frame_height = 0;
    frame_width = 0;
    video_saving_status = STOPPED;
    name_video_to_save = "";
    video_writer = nullptr;
    codec = 0;
}

CaptureThread::CaptureThread(QString videoPath, QMutex *lock) : running(false),
    cameraIndex(0),videoPath(videoPath), data_lock(lock)
{
    computingFps = false;
    fps = 0.0;

    //init video recording variables
    frame_height = 0;
    frame_width = 0;
    video_saving_status = STOPPED;
    name_video_to_save = "";
    video_writer = nullptr;
    codec = 0;
}

CaptureThread::~CaptureThread()
{

}


void CaptureThread::run()
{
    running = true;
    QString message = QString("Capturing camera %1").arg(cameraIndex);
    emit statusLabelChange(message);
    //capture picture frame from the camera
    cv::VideoCapture cap(cameraIndex);
    cv::Mat current_frame;

    frame_width = cap.get(cv::CAP_PROP_FRAME_WIDTH);
    frame_height = cap.get(cv::CAP_PROP_FRAME_HEIGHT);
    //codec = cap.get(cv::CAP_PROP_FOURCC);
    //codec = cv::CAP_OPENCV_MJPEG;
    codec = cv::VideoWriter::fourcc('m','p','4','v');
//    frame_width = 640;
//    frame_height = 480;

    while(running){
        cap >> current_frame;
        if(current_frame.empty()){
            break;
        }
        // proceed to record based on video saving status state
        if(video_saving_status == STARTING){
            startRecordingVideo(current_frame);
        }
        if(video_saving_status == STARTED){
            video_writer->write(current_frame); // output frame to current video record
        }
        if(video_saving_status == STOPPING){
            stopRecordingVideo();
        }

        // put to Red-Green-Blue order mode
        cv::cvtColor(current_frame, current_frame,cv::COLOR_BGR2RGB);

        //prevent with the mutex a data race between threads (write with this one and read by the GUI thread)
        data_lock->lock();
        frame = current_frame;
        data_lock->unlock();
        // emit signal to be caught by slot from the main GUI thread
        emit frameCaptured(&frame);

        // compute fps within the loop ~real time fps
        if(computingFps) {
            qInfo() << "INFO : thread compute fps setting is true, start calculating fps.";
            computeFPS(cap);
        }
    }

    // reset the message on status label to idle state message
    message = "Home Security is Ready";
    emit statusLabelChange(message);
    cap.release();
    running = false;
}


void CaptureThread::computeFPS(cv::VideoCapture &cap)
{
    const int count_to_read = 100;
    cv::Mat tmp_frame;
    QTime timer;

    timer.start();
    for(unsigned i=0;i<count_to_read;i++){
        cap >> tmp_frame;
    }
    const int elapsed_time_ms = timer.elapsed();
    fps = count_to_read / (elapsed_time_ms/1000.0); // [frame/sec]
    computingFps = false;
    emit fpsChanged(fps);
    qDebug() << "Debug : fps done in thread.";
}

void CaptureThread::startRecordingVideo(cv::Mat &firstFrame)
{
    name_video_to_save = Utilities::newSavedVideoName();

    // the first image will provide a thumbnail picture for the video in the GUI list of video recorded
    QString thumbnail = Utilities::getSavedVideoPath(name_video_to_save,"jpg");
    cv::imwrite(thumbnail.toStdString(), firstFrame);

//    video_writer = new cv::VideoWriter(
//        Utilities::getSavedVideoPath(name_video_to_save, "avi").toStdString(),
//        cv::VideoWriter::fourcc('M','J','P','G'),
//        fps = 16,
//        cv::Size(frame_width, frame_height));
    video_writer = new cv::VideoWriter();
    fps = 16;
    cv::Size Size = cv::Size(frame_width, frame_height);
    std::string filename = Utilities::getSavedVideoPath(name_video_to_save, "mp4").toStdString();
    //int codec = cv::VideoWriter::fourcc('D','V','I','X');
    video_writer->open(filename,
                      codec,
                      fps,
                       Size,
                       true); // color image
    // MJPG is motion-jpeg codec

//    if (!video_writer->isOpened()) {
//        qFatal("FATAL : Could not open the output video file for write.\n");
//        //return;
//    }

    video_saving_status = STARTED;
}

void CaptureThread::stopRecordingVideo()
{
    // cleaning
    video_saving_status = STOPPED;
    video_writer->release();
    delete video_writer;
    video_writer = nullptr;
    emit videoRecorded(name_video_to_save);
    std::string fileName = Utilities::getSavedVideoPath(name_video_to_save, "mp4").toStdString();
    const QString qfileName = QString::fromStdString(fileName);
    qInfo("INFO : video recording ended with video name to save %s", qPrintable(qfileName));
}
