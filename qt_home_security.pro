#-------------------------------------------------
#
# Project created by QtCreator 2023-11-09T19:55:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt_home_security
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        CaptureThread.cpp \
        Utilities.cpp \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        CaptureThread.h \
        Utilities.h \
        mainwindow.h

QT += multimedia


INCLUDEPATH += C:\OpenCV4\opencv\Release\install\include

LIBS += C:\OpenCV4\opencv\Release\bin\libopencv_core480.dll
LIBS += C:\OpenCV4\opencv\Release\bin\libopencv_highgui480.dll
LIBS += C:\OpenCV4\opencv\Release\bin\libopencv_imgcodecs480.dll
LIBS += C:\OpenCV4\opencv\Release\bin\libopencv_imgproc480.dll
LIBS += C:\OpenCV4\opencv\Release\bin\libopencv_calib3d480.dll
LIBS += C:\OpenCV4\opencv\Release\bin\libopencv_videoio480.dll


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
